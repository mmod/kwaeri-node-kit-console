# [![Patreon](https://img.shields.io/badge/Patreon-Funding-inactive?style=for-the-badge&logo=patreon&color=FF424D)](https://patreon.com/kirvedx) kwaeri-node-kit-console [![PayPal](https://img.shields.io/badge/PayPal-Donations-inactive?style=for-the-badge&logo=paypal&color=253B80)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)

A Massively Modified Open Source Project by kirvedx

[![GPG/Keybase](https://img.shields.io/badge/GPG-1B842CB5%20Rik-inactive?style=for-the-badge&label=GnuPG2%2FKeybase&logo=gnu+privacy+guard&color=0093dd)](https://keybase.io/rik)
[![Google](https://img.shields.io/badge/Google%20Developers-kirvedx-inactive?style=for-the-badge&logo=google+tag+manager&color=414141)](https://developers.google.com/profile/u/117028112450485835638)
[![GitLab](https://img.shields.io/badge/GitLab-kirvedx-inactive?style=for-the-badge&logo=gitlab&color=fca121)](https://github.com/kirvedx)
[![GitHub](https://img.shields.io/badge/GitHub-kirvedx-inactive?style=for-the-badge&logo=github&color=181717)](https://github.com/kirvedx)
[![npm](https://img.shields.io/badge/NPM-Rik-inactive?style=for-the-badge&logo=npm&color=CB3837)](https://npmjs.com/~rik)

The @kwaeri/console component for the @kwaeri/node-kit application platform

[![pipeline status](https://gitlab.com/kwaeri/node-kit/console/badges/main/pipeline.svg)](https://gitlab.com/kwaeri/node-kit/console/commits/main)  [![coverage report](https://gitlab.com/kwaeri/node-kit/console/badges/main/coverage.svg)](https://kwaeri.gitlab.io/node-kit/console/coverage/)  [![CII Best Practices](https://bestpractices.coreinfrastructure.org/projects/1879/badge)](https://bestpractices.coreinfrastructure.org/projects/1879)

## TOC
* [The Implementation](#the-implementation)
* [Getting Started](#getting-started)
  * [Installation](#installation)
  * [Usage](#usage)
* [How to Contribute Code](#how-to-contribute-code)
* [Other Ways to Contribute](#other-ways-to-contribute)
  * [Bug Reports](#bug-reports)
  * [Vulnerability Reports](#vulnerability-reports)
    * [Confidential Issues](#confidential-issues)
  * [Donations](#donations)

## The Implementation

@kwaeri/console provides a system to easily style text that is to be sent to the JavaScript console.

The @kwaeri/console component offers an eloquent API featuring methods for setting background color, foreground color, and text effects. Additional methods offer a means to buffer portions of text, to manipulate the styles of each portion, and to dump the result as a collective whole.

## Getting Started

The following sections cover how to get started with @kwaeri/console. For a more formal experience, browse the [online documentation](https://kwaeri.gitlab.io/node-kit/console/).

### Installation

Start off by installing the module in your project:

```bash
npm install @kwaeri/console
```

### Usage

To leverage the console module, you'll first need to include it:

```javascript
// INCLUDES
import { Console } from '@kwaeri/console';
```

Follow up by creating an instance of the Console object:

```javascript
// DEFINES
const output = new Console( { color: false, background: false, decor: [] } );
```

Now you're ready to start formatting text.

#### Color

To set the color of any subsequently buffered text, use the **color** method:

```javascript
output.color( 'blue' );
```

The following colors are available to choose from:

* black
* red
* green
* yellow
* blue
* magenta
* cyan
* white
* gray

#### Background

To set the background color of any subsequently buffered text, use the **background** method:

```javascript
output.background( 'black' );
```

The following background colors are available to choose from:

* black
* red
* green
* yellow
* blue
* magenta
* cyan
* white

#### Decor

To apply a variety of text decorations to any subsequently buffered text, use the **decor** method:

```javascript
output.decor( ['bright'] );
```

The following text decorations are available to choose from:

* bright
* dim
* underline
* blink (Probably isn't going to work for you...)
* reverse
* hidden

#### Normalize

When you want to reset the state of the styling, use the **normalize** method:

```javascript
output.normalize();
```

This will clear the background and foreground colors, as well as clear and text decorations.

#### Clear

When you want to clear any buffered text, use the **clear** method:

```javascript
output.clear();
```

The clear method clears the stack of buffered text.

#### Buffer

When you want to buffer portions of styled text, use the **buffer** method:

```javascript
output.buffer( 'This is some sample text' );
```

Each call to buffer adds a layer to the stack.

#### Dump

When you want to combine the portions of text which have been buffered, use the **dump** method:

```javascript
console.log( output.dump() );
```

Each layer of the stack is joined to form a string. Once the stack has been dumped, it has also been cleared, so you are left with a fresh slate.

Keep in mind, however, that the styling does not normalize from a call to `dump()`.

#### Putting it Together

Here's an example where we put it all together to create a colorful message for the console:

```javascript
// INCLUDES
import { Console } from '@kwaeri/console';


// DEFINES
let output = new Console( { color: false, background: false, decor: [] } );


// Here we create an error message for a module that takes command line arguments:
let errorMessage = output.color( 'red' ).decor( ['bright'] ).buffer( '[ERROR]' )
.normalize().buffer( ': Invalid Command. \'' + quest + '\' is not a valid command. See ' )
.color( 'blue' ).decor( ['bright'] ).buffer( '--help ' )
.normalize().buffer( 'for a list of commands you can execute.' )
.dump();

console.log( errorMessage );

// The output object is normalized if you couldn't tell, so we have a fresh slate
// to continue with...
```

## How to Contribute Code

Our Open Source projects are always open to contribution. If you'd like to cocntribute, all we ask is that you follow the guidelines for contributions, which can be found at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Contribute-Code)

There you'll find topics such as the guidelines for contributions; step-by-step walk-throughs for getting set up, [Coding Standards](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Coding-Standards), [CSS Naming Conventions](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/CSS-Naming-Conventions), and more.

The project also leverages Keybase for communication and alerts - outside of standard email. To join our keybase chat, run the following from terminal (assuming you have [keybase](https://www.keybase.io) installed and running):

```bash
keybase team request-access kwaeri
```

Alternatively, you could search for the team in the GUI application and request access from there.

## Other Ways to Contribute

There are other ways to contribute to the project other than with code. Consider [testing](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Test-Code) the software, or in case you've found an [Bug](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports) - please report it. You can also support the project monetarly through [donations](https://gitlab.com/mmod/documentation/wikis/Contribute-to-Massively-Modified/Donations) via PayPal.

Regardless of how you'd like to contribute, you can also find in-depth information for how to do so at the [Massively Modified Wiki](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute)

### Bug Reports

To submit bug reports, request enhancements, and/or new features - please make use of the **issues** system baked-in to our source control project space at [Gitlab](https://gitlab.com/mmod/kwaeri-node-kit/issues)

You may optionally start an issue, track, and manage it via email by sending an email to our project's [support desk](mailto:incoming+mmod/kwaeri-node-kit@incoming.gitlab.com).

For more in-depth documentation on the process of submitting bug reports, please visit the [Massively Modified Wiki on Bug Reports](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Bug-Reports)

### Vulnerability Reports

Our Vulnerability Reporting process is very similar to Gitlab's. In fact, you could say its a *fork*.

To submit vulnerability reports, please email our [Security Group](mailto:security@mmod.co). We will try to acknowledge receipt of said vulnerability by the next business day, and to also provide regular updates about our progress. If you are curious about the status of your report feel free to email us again. If you wish to encrypt your disclosure email, like with gitlab - please email us to ask for our GPG Key.

Please refrain from requesting compensation for reporting vulnerabilities. We will publicly acknowledge your responsible disclosure, if you request us to do so. We will also try to make the confidential issue public after the vulnerability is announced.

You are not allowed, and will not be able, to search for vulnerabilities on Gitlab.com. As our software is open source, you may download a copy of the source and test against that.

#### Confidential Issues

When a vulnerability is discovered, we create a [confidential issue] to track it internally. Security patches will be pushed to private branches and eventually merged into a `security` branch. Security issues that are not vulnerabilites can be seen on our [public issue tracker](https://gitlab.com/mmod/kwaeri-node-kit/issues?label_name%5B%5D=Security).

For more in-depth information regarding vulnerability reports, confidentiality, and our practices; Please visit the [Massively Modified Wiki on Vulnerability](https://gitlab.com/mmod/documentation/wikis/Other-Ways-to-Contribute/Vulnerability-Reports)

### Donations

If you cannot contribute time or energy to neither the code base, documentation, nor community support; please consider making a monetary contribution which is extremely useful for maintaining the Massively Modified network and all the goodies offered free to the public.

[![Donate via PayPal.com](https://gitlab.com/mmod/kwaeri-user-experience/raw/master/images/mmod-donate-btn-2.png)](https://www.paypal.com/cgi-bin/webscr?cmd=_s-xclick&hosted_button_id=YUW4CWCAABCU2)
