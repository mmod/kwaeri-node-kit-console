/**
 * SPDX-PackageName: kwaeri/console
 * SPDX-PackageVersion: 2.3.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


 'use strict'


 // INCLUDES


 // DEFINES
export type ConsoleBits = {
    background: boolean|string,
    color: boolean|string,
    decor: Array<string>
}


/**
 * Console class definition
 */
export class Console {

    /**
     * @var { ConsoleBits }
     */
    config: ConsoleBits = {
        background: false,
        color: false,
        decor: []
    }


    /**
     * @var { Array<string }
     */
    stack: Array<string> = [];


    /**
     * @var { string }
     */
    reset  = "\x1b[0m"


    /**
     * @var { any }
     */
    decorations = {
        bright:     "\x1b[1m",
        dim:        "\x1b[2m",
        underline:  "\x1b[4m",
        blink:      "\x1b[5m",
        reverse:    "\x1b[7m",
        hidden:     "\x1b[8m"
    }


    /**
     * @var { any } colors
     */
    colors = {
        black:  "\x1b[30m",
        red:    "\x1b[31m",
        green:  "\x1b[32m",
        yellow: "\x1b[33m",
        blue:   "\x1b[34m",
        magenta:"\x1b[35m",
        cyan:   "\x1b[36m",
        white:  "\x1b[37m",
        gray: "\x1b[90m"
    };


    /**
     * @var { any } backgroundColors
     */
    backgroundColors = {
        black:  "\x1b[40m",
        red:    "\x1b[41m",
        green:  "\x1b[42m",
        yellow: "\x1b[43m",
        blue:   "\x1b[44m",
        magenta:"\x1b[45m",
        cyan:   "\x1b[46m",
        white:  "\x1b[47m"
    };


    /**
     * Class constructor
     *
     * @param { consoleBits } configuration
     */
    constructor( configuration: ConsoleBits ) {
        // Ensure we set the color if it is passed in the config:
        this.color( configuration.color );

        // Same for the background:
        this.background( configuration.background );

        // As well as for the decor:
        this.decor( configuration.decor );
    }


    /**
     * Sets the color for any subsequently buffered text.
     *
     * @param { boolean|string } color
     *
     * @returns { this } A reference to this {@link console}, to allow chaining
     */
    color( color: boolean|string ): Console {
        // Reset the color:
        this.config.color = false;

        // Set the color if a valid color parameter is present;
        if( color && typeof color == "string" && color !== "" )
            this.config.color = ( this.colors as any )[color];

        // Return this instance to allow chaining:
        return this;
    }


    /**
     * Sets the background color for any subsequently buffered text
     *
     * @param { boolean|string } color
     *
     * @returns { this } A reference to this {@link console}, to allow chaining
     */
    background( color: boolean|string ): Console {
        // Reset the background color:
        this.config.background = false;

        // Set the background color if a valid color parameter is present:
        if( color && typeof color == "string" && color !== "" )
            this.config.background = ( this.backgroundColors as any )[color];

        // Return this instance to allow chaining:
        return this;
    }


    /**
     * Sets any decorative flags for any subsequent text
     *
     * @param { Array<string> } textDecorations An array of text-decoration flags
     *
     * @returns { this } A reference to this {@link console}, to allow chaining
     */
    decor( textDecorations: Array<string> ): Console {
        // Reset the options array:
        this.config.decor = [];

        // Loop through provided options and set any that are valid:
        if( textDecorations && textDecorations.length ) {
            for( let decoration in textDecorations ) {
                this.config.decor.push( ( this.decorations as any )[textDecorations[decoration]] );
            }
        }

        // Return this instance to allow chaining:
        return this;
    }


    /**
     * Resets the text color and style for any subsequently buffered text
     *
     * @param { void }
     *
     * @returns { this } A reference to this {@link console}, to allow chaining
     */
    normalize(): Console {
        // Reset the color:
        this.config.color = false;

        // Reset the background:
        this.config.background = false;

        // Reset the reset text decorations:
        this.config.decor = [];

        // Return this instance to allow chaining:
        return this;
    }


    /**
     * Buffers provided text using any currently set colors and styles, and adds it to a stackfor later use
     *
     * @param { string } text The text being colored/styled
     *
     * @returns { this } A reference to this {@link console}, to allow chaining
     */
    buffer( text: string ): Console {
        // Prepare a temporary container for the text we'll be buffering:
        let output = "";

        // Set a fresh style state for the text:
        output += this.reset;

        // Start with decor:
        if( this.decor.length ) {
            for( let decoration in this.decor ) {
                output += ( this.config.decor as any)[decoration];
            }
        }

        // Follow with background color:
        if( this.config.background )
            output += this.config.background;

        // Follow with color:
        if( this.config.color )
            output += this.config.color;

        // Follow with the provided text:
        if( text && typeof text == "string" && text !== "" )
            output += text;

        // Follow with yet another reset?  Did we really do this?
        output += this.reset;

        // Add the buffer to the stack:
        this.stack.push( output );

        // Return this instance to allow chaining:
        return this;
    }


    /**
     * Empties the stack; joining each layer, and returning the result.
     *
     * @returns { string } THe buffered string
     */
    dump(): string {
        // Assign the stack to a buffer so that we can reset the stack
        const buffered = this.stack;

        // Reset the stack
        this.stack = [];

        // Return the buffered output
        return buffered.join( '' );
    }


    /**
     * Clears the stack
     *
     * @returns { this } A reference to this {@link console}, to allow chaining
     */
    clear(): Console {
        // Set the stack to an empty array
        this.stack = [];

        // Return this instance to allow chaining:
        return this;
    }
}
