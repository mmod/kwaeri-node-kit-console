/**
 * SPDX-PackageName: kwaeri/console
 * SPDX-PackageVersion: 2.3.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


'use strict'

// INCLUDES
import * as assert from 'assert';
import { Console } from '../src/console.mjs';
import debug from 'debug';


// DEFINES
const DEBUG = debug( 'nodekit:console' );
const output = new Console( { color: false, background: false, decor: [] } );

// SANITY CHECK - Makes sure our tests are working proerly
describe(
    'PREREQUISITE',
    () => {
        describe(
            'Sanity Test(s)',
            () => {

                it(
                    'Should return true.',
                    () => {
                        assert.equal( [1,2,3,4].indexOf(4), 3 );
                    }
                );

            }
        );
    }
);


describe(
    'Console Functionality Test Suite',
    () => {

        describe(
            'Set Color Test',
            () => {
                it(
                    'Should return true, indicating text color stacked.',
                    () => {
                        // Set the color to blue:
                        output.color( 'blue' );

                        // Show the configured color value:
                        DEBUG( [output.config.color] );

                        // Test that the configured color value is what we want:
                        assert.equal( output.config.color, "\x1b[34m" );
                    }
                );
            }
        );


        describe(
            'Set Background Color Test',
            () => {
                it(
                    'Should return true, indicating background color stacked.',
                    () => {
                        // Set the background color to black:
                        output.background( 'black' );

                        // Show the configured background color value:
                        DEBUG( [output.config.background] );

                        // Test that the configured background color value is what we want:
                        assert.equal( output.config.background, "\x1b[40m" );
                    }
                );
            }
        );


        describe(
            'Set Text Decoration Test',
            () => {
                it(
                    'Should return true, indicating text decoration stacked.',
                    () => {
                        // Set the decor to only bright:
                        output.decor( ['bright'] );

                        // Show the configured decor:
                        DEBUG( [output.config.decor] );

                        // Test that the configured decor value is what we want:
                        assert.equal( output.config.decor[0], "\x1b[1m" );
                    }
                );
            }
        );


        describe(
            'Buffer Text Test',
            () => {
                it(
                    'Should return true, indicating text was stacked.',
                    () => {
                        // Buffer some text:
                        output.buffer( "This is some sample text" );

                        // Check that the stack has a length:
                        DEBUG( [output.stack] );

                        // Test the content of the stack, based upon the styling applied:
                        assert.equal( output.stack[0], '\u001b[0m\u001b[40m\u001b[34mThis is some sample text\u001b[0m'  );
                    }
                );
            }
        );


        describe(
            'Dump Text Test',
            () => {
                it(
                    'Should return true, indicating an emptied stack after text dumped.',
                    () => {
                        // Dump the stack to a string:
                        const string = output.dump();

                        // Show the string:
                        DEBUG( [string] );

                        // Test that the string had a lenth, and that the stack is now empty:
                        assert.equal( ( output.stack.length == 0 && string.length > 0 ), true  );
                    }
                );
            }
        );


        describe(
            'Normalize Text Test',
            () => {
                it(
                    'Should return true, indicating all configuration arrays are empty.',
                    () => {
                        // Clear styling:
                        output.normalize();

                        // Check the state of styling:
                        DEBUG( [output.config] );

                        // Test that styling has been reset:
                        assert.equal( ( output.config.color == false && output.config.background == false && output.config.decor.length == 0 ), true  );
                    }
                );
            }
        );


        describe(
            'Clear Buffer Test',
            () => {
                it(
                    'Should return true, indicating an existing and populated stack was cleared.',
                    () => {
                        // Buffer some text:
                        output.buffer( 'This is another sample string' );

                        // Check the length of the stack:
                        let stackCheckGood = ( output.stack.length > 0 ) ? true: false;

                        // Clear the stack:
                        output.clear();

                        // Show the current state of the stack:
                        DEBUG( output.stack );

                        // Run a test to see if the stack had any content prior to clearing it,
                        // as well as whether the stack has a length of 0 - as desired - after clearing it:
                        assert.equal( ( stackCheckGood && output.stack.length == 0 ), true  );
                    }
                );
            }
        );

    }
);



