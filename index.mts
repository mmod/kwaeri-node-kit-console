/**
 * SPDX-PackageName: kwaeri/console
 * SPDX-PackageVersion: 2.3.0
 * SPDX-FileCopyrightText: © 2014 - 2022 Richard Winters <kirvedx@gmail.com> and contributors
 * SPDX-License-Identifier: Apache-2.0 WITH LLVM-exception OR MIT
 */


 'use strict'


 // INCLUDES

 // ESM WRAPPER
 export {
    Console
} from './src/console.mjs';

